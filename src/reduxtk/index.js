import {combineReducers} from 'redux'
import results from './resultsSlice'

const gitlabStore = combineReducers({
  results
})

export default gitlabStore