import {combineReducers} from 'redux'
import results from './ResultsReducer'

const gitlabStore = combineReducers({
  results
})

export default gitlabStore