import React from 'react';

import {createStore} from 'redux'
import { configureStore } from "@reduxjs/toolkit";
import AppStore from './redux/reducers'
import AppStoreTK from './reduxtk'
import { Provider } from 'react-redux'

import Search from './components/Search'
import Results from './components/Results'
import './App.css';

// const store = createStore(AppStore,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const store = configureStore({reducer: AppStoreTK})

function App() {
  return (
    <Provider store={store} >
      <div className="container">
        <h1>GitLab Search</h1>
        <Search />
        <Results />
      </div>
    </Provider>
  );
}

export default App;
