import { createSlice } from '@reduxjs/toolkit'

const resultsSlice = createSlice({
  name: 'results',
  initialState: {searchText: '', data: []},
  reducers: {
    setResults: function(state, action){ return state = action.payload },
    clearResults: function(state, action){ return state = {searchText: '', data: []}}
  }
})

export const {setResults, clearResults} = resultsSlice.actions

export default resultsSlice.reducer