import React, {useEffect} from 'react'
import { useSelector } from "react-redux";

export default function Results() {
  const results = useSelector(s => s.results)

  useEffect(() => {
    console.log((results))
  }, [results])

  return (
    <div >  
      <div className='resultsContainer'>
         Results: {results.data.length}
        {results.data.map(result => <div className='results'>
            <div><span>filename:</span> <span>{result.filename}</span></div>
            <div><span>project:</span> <span><a href='#' onClick={() => window.open(result.url)} style={{color:'white'}}>{result.project_id}</a></span></div>
            <div><span>line:</span> <span>{result.line}</span></div>
            <div><span>text:</span> <div>{result.text.replace(results.searchText, <span className='resultText'>results.searchText</span>)}</div></div>
          </div>)}
      </div>
    </div>
  )
}
