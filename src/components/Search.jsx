import React, {useState} from 'react'
import { useDispatch } from 'react-redux'
// import { SET_RESULTS, CLEAR_RESULTS } from '../redux/actions/resultsActions'
import { clearResults, setResults } from "../reduxtk/resultsSlice";

import Axios from 'axios'

export default function Search() {
  const [text, setText] = useState('Enter Search')
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()

  const search = (event, text) => {
    event.preventDefault()
    setLoading(true)
    // dispatch({type: CLEAR_RESULTS})
    dispatch(clearResults())
    Axios.post('/api/search/', {searchText: text})
      .then(results => {
        // dispatch({type: SET_RESULTS, payload: results.data })
        dispatch(setResults(results.data))
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <div>
      <form className='search' onSubmit={e => search(e, text)}>
        {loading 
          ? (<h1>Searching...</h1>)
          : (<input 
            onChange={e => setText(e.target.value)}
            placeholder={text}
            disabled={loading}
          />)
        }
      </form>
    </div>
  )
}
