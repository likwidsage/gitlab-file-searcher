require('dotenv').config()
const app = require('express').Router()
const axios = require('axios')

app.get('/', (req, res) => {
  console.log('reached')
  return res.status(200).json({ message: 'OK' })
})

app.post('/', (req, res) => {
  const { searchText } = req.body,
    glauth = process.env.GITLAB_AUTH,
    gitlab_url = process.env.GITLAB_URL,
    projectIds = process.env.PROJECT_IDS.split(',')

  if (!searchText) {
    return res.status(400).json({ message: 'Missing searchText!' })
  }

  if (!glauth) {
    return res.status(400).json({ message: 'Missing glauth!' })
  }

  if (!gitlab_url) {
    return res.status(400).json({ message: 'Missing gitlab_url!' })
  }

  if (!projectIds || !Array.isArray(projectIds) || projectIds.lenth === 0) {
    return res.status(400).json({ message: 'Missing projectIds or invalid array!' })
  }

  console.log(`searching ${searchText} with auth ${glauth}`)

  const config = {
    headers: {
      'Authorization': `Bearer ${glauth}`,
      'Accept': '*/*',
      'Cache-Control': 'no-cache',
      'Host': 'gitlab01.promedica.org',
      'Accept-Encoding': 'gzip, deflate, br',
      'Connection': 'keep-alive'
    }
  }

  let allProjects = []
  let results = []
  Promise.all(
    projectIds.map(projectId => axios.get(`${gitlab_url}/api/v4/groups/${projectId}/projects?simple=true&page=1`, config))
  )
    .then(responses => {
      responses.forEach(response => {
        response.data.forEach(project => allProjects.push(project))
      })
      return allProjects
    })
    .then(allProjects => {
      return Promise.all(allProjects.map(projectData =>
        axios.get(`${gitlab_url}/api/v4/projects/${projectData.id}/search?scope=blobs&search=${searchText}`, config)
      ))
    })
    .then(responses => {
      responses.forEach(response => response.data.forEach(
        result => results.push(result)
      ))
      
      const data = results.map(result => {
        return {
            filename: result.filename,
            project_id: result.project_id,
            url: allProjects.filter(project => project.id === result.project_id)[0].web_url,
            line: result.startline,
            text: result.data
          }
      })
      
      const returnData = { searchText, data }
      console.log('results:', returnData)
      return res.status(200).json(returnData)
    })
    .then(results => {
    })
    .catch(err => {
      console.log(err)
      res.status(500).json(err)
    })

})

module.exports = app