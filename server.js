const express = require('express'), 
  app = express()
const bodyParser = require('body-parser')

const cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const search = require('./api/Search')
app.use('/api/search', search)

app.use(express.static('build'))
const path = require('path')
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'build', 'index.html'))
})

const port = 5000
app.listen(port, () => {
  console.log(`API on port ${port} in ${app.get('env')} mode`)
})