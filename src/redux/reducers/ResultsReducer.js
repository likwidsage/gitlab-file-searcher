import { SET_RESULTS, CLEAR_RESULTS } from '../actions/resultsActions'

const initialState = {searchText: '', data: []}

export default function resultsReducer(state = initialState, action){
  switch (action.type) {
    case SET_RESULTS:
      return action.payload
    case CLEAR_RESULTS:
      return initialState
    default:
      return state
  }
}