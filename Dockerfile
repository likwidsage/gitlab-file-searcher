FROM node:8.16-alpine
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build --production
EXPOSE 5000
CMD ["node", "server.js"]
